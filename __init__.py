from sys import argv
from modules import *

if __name__ == '__main__':
	if len(argv) > 1:
		treasureMap = Map(filePath=argv[1])
		# FIXME: fazer a leitura do arquivo e montar o map aqui
		# 		  para que o Indiana receba o gamma		
		if len(argv) > 2:
			game = GameController(treasureMap,argv[2])
		else:
			game = GameController(treasureMap)
		game.start()
		
	else :
		print "Invalid Map: Check your file, please."