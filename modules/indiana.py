from .map import Map
from sys import exit
import numpy

class Indiana():
	def __init__(self,gameController,adventureMap):
		self.map = adventureMap		
		self.position = (0,len(self.map.places)-1)
		self.steps = 0
		self.oraculo = gameController
		self.treasure = self.oraculo.getReward(self.position)		
		self.gamma = self.map.gamma
		self.penaltyForOutOfBounds = -abs(self.map.rewardDefault*2)

	def step(self,direction):						
		failChance = numpy.random.ranf()
		# 20% chance of move fails
		if failChance > 0.8 :			
			direction = self.getRandomOrtogonalDirection(direction)

		nextPosition = self.directionToPosition(direction)		
		print 'Indiana says: I\'m going to position ' + str(nextPosition)
		if self.oraculo.isInBounds(nextPosition):
			if self.oraculo.isValidStep(nextPosition):								
				newReward = self.oraculo.getReward(nextPosition)
				currentReward = self.oraculo.getReward(self.position)
				self.map.markReward(self.position,currentReward)		
				
				self.learn(self.position,direction,nextPosition,newReward)				
				self.position = nextPosition								
				# self.learn(nextPosition,direction,nextPosition,newReward)
				self.map.markReward(self.position,newReward)		
				self.treasure += newReward				
			else:				
				self.learn(self.position,direction,self.position,self.penaltyForOutOfBounds)				
				self.map.markInvalid(nextPosition)		
		else:
			self.learn(self.position,direction,self.position,self.penaltyForOutOfBounds)
			print 'Oracle says: \"Out of Boundaries, dear adventurer.\"'
		self.steps += 1				

	def walk(self,episode):					
		print 'Indiana says: my position is ' + str(self.position)
		knownActions = self.map.getActions(self.position)		
		exploreChance = numpy.random.ranf()		
		mapSize = self.map.columnsCount*self.map.linesCount
		if mapSize == 0:
			exit('Oraculo says: where is my map?! (map size is 0)')

		# epsilon = numpy.tanh(mapSize/episode) # calcular utilizando o tamanho do mapa		
		epsilon = float(mapSize)/(episode+mapSize)
		self.learningRate = 0.2+epsilon
		exploreRate = epsilon
		# print epsilon

		if knownActions != {}:
			goodDirection, _ = max(knownActions.iteritems(), key=lambda x:x[1])		
		else:
			goodDirection = None
		if exploreChance < exploreRate or knownActions == {}:
			allActions = ['up','right','down','left']
			if goodDirection != None :
				allActions.remove(goodDirection)

			randomDirection = numpy.random.random_integers(low=0,high=len(allActions)-1)				
			direction = allActions[randomDirection]		
			print 'Indiana says: I laugh in face of danger!'		
		else:					
			direction = goodDirection
		
		# print direction

		self.step(direction)										

	def learn(self,currentPosition,direction,nextPosition,reward):				
		knownActions = self.map.getActions(nextPosition)	
		# print knownActions		
		if len(knownActions) > 0 :	
			_, maxQAround = max(knownActions.iteritems(), key=lambda x:x[1])				 
		else:
			maxQAround = 0	

		currentQ = self.map.getQ(currentPosition,direction)
		currentQ = currentQ if currentQ != None else 0
		newValue = ((1-self.learningRate)*currentQ) + (self.learningRate * (reward + (self.gamma * maxQAround)))

		self.map.markQ(currentPosition,direction,newValue)						

	def directionToPosition(self,direction):
		if direction == 'up' :
			position = (self.position[0],self.position[1]-1)
		elif direction == 'right' :
			position = (self.position[0]+1,self.position[1])
		elif direction == 'down' :
			position = (self.position[0],self.position[1]+1)
		elif direction == 'left' :
			position = (self.position[0]-1,self.position[1])
		else:
			exit('Indiana says: this is not a direction, mate.')
		return position

	def getRandomOrtogonalDirection(self,direction):
		randomChance = numpy.random.ranf()
		allActions = ['up','right','down','left']
		directionIndex = allActions.index(direction)
		# 10% chance for each random ortogonal move
		if randomChance > 0.5:			
			ortogonalIndex = directionIndex-1 if directionIndex > 0 else len(allActions)-1
		else:
			ortogonalIndex = directionIndex+1 if directionIndex < len(allActions)-1 else 0
		
		randomOrtogonalDirection = allActions[ortogonalIndex]				
		print 'Indiana says: I failed when going ' + direction +'! I went to ' + randomOrtogonalDirection
		return randomOrtogonalDirection