from indiana import Indiana
import datetime, time

class GameController():
	def __init__(self,treasureMap,lastEpisode=100):
		self.lastEpisode = int(lastEpisode)
		self.map = treasureMap
		rawMap = self.map.generateRawMap()
		self.explorer = Indiana(self,rawMap)		
		self.maxTreasure = None
		self.episode = 0

	def start(self):		
		currentTime = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')
		outputFile = open("./output/%s.csv" % currentTime, "w+")
		self.output = outputFile
		self.output.write('Episode,Steps,Treasure\n')
		for episode in xrange(1,self.lastEpisode+1):	
			print 'Episode '+ str(episode)						
			self.episode = episode
			isFinished = False
			while not(isFinished):				
				print 'Step ' + str(self.explorer.steps)
				self.explorer.walk(episode)
				self.explorer.map.show()
				isFinished = self.checkFinalPosition(self.explorer.position)			
		if self.maxTreasure != None:
			print 'Max Treasure ' + str(self.maxTreasure[1]) +' (ep '+str(self.maxTreasure[0])+')'		
		else:
			print 'Oracle says: You didn\'t reach any treasure, Indiana.'

	def isValidStep(self,position):	
		return self.map.places[position[1]][position[0]].valid
		
	def isInBounds(self,position):
		pos_x = position[0]
		pos_y = position[1]
		places = self.map.places
		checkVerticalBounds = pos_y >= 0 and pos_y < len(places)		
		if checkVerticalBounds:
			checkHorizontalBounds = pos_x >= 0 and pos_x < len(places[pos_y])			
			return  checkHorizontalBounds and checkVerticalBounds		
		return False
			
	def getReward(self,position):
		return self.map.getReward(position)

	def checkFinalPosition(self,position):
		if self.getReward(position) != self.map.rewardDefault:
			print 'Oracle says: You end your journey, Indiana.'			
			print 'Treasure is ' + str(self.explorer.treasure)
			if self.maxTreasure == None or self.explorer.treasure > self.maxTreasure[1]:
				self.maxTreasure = (self.episode,self.explorer.treasure)				
			
			self.output.write(str(self.episode) + ',' + str(self.explorer.steps) + ',' + str(self.explorer.treasure) +'\n')
			self.explorer.treasure = 0
			self.explorer.steps = 0
			self.explorer.position = (0,self.map.linesCount-1)
			return True
		else:
			return False