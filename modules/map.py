from sys import exit
import copy

class Square():	
	def __init__(self,isValid=False,value=0.0):
		self.valid = isValid
		self.value = value
		self.actions = { }

class Map(object):	
	def __init__(self, filePath='',gamma=0.5,rewardDefault=0.0,places=[]):
		if filePath != '':
			readFile = open(filePath, 'r')
			attributes = readFile.read().split('\n')        		
			# initialzie counters, default reward and gamma
			self.linesCount = int(attributes[0].split('=')[1])
			self.columnsCount = int(attributes[1].split('=')[1])
			self.rewardDefault = float(attributes[2].split('=')[1])
			self.gamma = float(attributes[3].split('=')[1])
			# convert file to map initializing self.places
			beginValidLines = 4
			endValidLines = beginValidLines+self.linesCount
			beginValueLines = endValidLines
			endValueLines = beginValueLines+self.linesCount
			strLines = [x.split(' ') for x in attributes[beginValidLines:endValidLines]]
			valLines = 	[y.split(' ') for y in attributes[beginValueLines:endValueLines]]		
			self.places  = self.fileFormatValueToMap(valLines,strLines)		
			self.check_lines()
		else:
			self.gamma = gamma
			self.rewardDefault = rewardDefault
			self.places = places		

	def fileFormatValueToMap(self,valueLines,validLines):		
		squareValues = []
		newSquares = []
		for i in range(0,self.linesCount):
			for j in range(0,self.columnsCount):				
				if valueLines[i][j] == 'D' or valueLines[i][j] == 'd':
					value = self.rewardDefault
				elif valueLines[i][j] == 'X' or valueLines[i][j] == 'x':					
					# value = float('-inf')
					value = 0.0
				else:
					value = float(valueLines[i][j])
				valid = (validLines[i][j] != 'X' and validLines[i][j] != 'x')
				newSquares.append(Square(isValid=valid,value=value))
		newSquares = [newSquares[x:x+self.columnsCount] for x in xrange(0,len(newSquares),self.columnsCount)]					
		return newSquares

	def check_lines(self):		
		if len(self.places) != self.linesCount:
			exit("Invalid formated map in file. Check, please.")
			
		for placesInLine in self.places:
			if len(placesInLine) != self.columnsCount:				
				exit("Invalid formated map in file. Check, please.")

	def show(self):	
		toPrint = 'Current map:\n'	
		for placesInLine in self.places:			
			for place in placesInLine:
				if not(place.valid):
					toPrint += ' X  '
				else:
					toPrint += str(place.value) + ' '
			toPrint += '\n'

		print toPrint

	def markQ(self,position,direction,QValue):		
		self.places[position[1]][position[0]].actions[direction] = QValue

	def markReward(self,position,reward):	
		self.places[position[1]][position[0]].value = reward		

	def markInvalid(self,position):				
		self.places[position[1]][position[0]].valid = False		

	def generateRawMap(self):
		rawMap = copy.deepcopy(self)		

		for i in range(0,len(rawMap.places)):			
			for j in range(0,len(rawMap.places[i])):
				# if rawMap.places[i][j].value == self.rewardDefault:
				# 	rawMap.places[i][j].value = 0.0
				rawMap.places[i][j].value = 0.0
				rawMap.places[i][j].actions = {'up':None,'right':None,'down':None, 'left':None}
				if not(rawMap.places[i][j].valid):
					rawMap.places[i][j].valid = True		
		self.show()
		rawMap.show()
		return rawMap

	def getPlacesAround(self,position):
		placesAround = []
		pos_x = position[0]
		pos_y = position[1]
		if pos_x > 0:
			placesAround.append([(pos_x-1,pos_y),self.places[pos_y][pos_x-1]])
		if pos_x < len(self.places[pos_y])-1:
			placesAround.append([(pos_x+1,pos_y),self.places[pos_y][pos_x+1]])
		if pos_y > 0:
			placesAround.append([(pos_x,pos_y-1),self.places[pos_y-1][pos_x]])
		if pos_y < len(self.places)-1:
			placesAround.append([(pos_x,pos_y+1),self.places[pos_y+1][pos_x]])
		return placesAround
	
	def getQ(self,position,direction):
		actions = self.places[position[1]][position[0]].actions
		return actions[direction] if direction in actions else None

	def getActions(self,position):
		return dict(x for x in self.places[position[1]][position[0]].actions.items() if x[1] != None)

	def getValuesAround(self,position):		
		return [x[1].value for x in self.getPlacesAround(position) if x[1].valid] 
	
	def getReward(self,position):
		return self.places[position[1]][position[0]].value		
